import { data } from "../../data/data.js";
import css from "./Table.module.scss";

export const Table = () => {
  return (
    <div>
      <h3>Exchange rate for {data[0].exchangedate}</h3>
      <table>
        <thead>
          <tr>
            <th>Name of rate</th>
            <th>Rate</th>
            <th>CC</th>
          </tr>
        </thead>
        <tbody>
          {data.map((el, cc) => {
            return (
              <tr className={css.trow} key={cc}>
                <td>{el.txt}</td>
                <td>{el.rate.toFixed(3)}</td>
                <td>{el.cc}</td>
              </tr>
            );
          })}
        </tbody>
      </table>
    </div>
  );
};
